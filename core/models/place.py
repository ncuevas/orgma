# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from core.models import Instructor
from django.utils import timezone
from cities_light.models import Country, Region

class Place(models.Model):
    """
    Place model class.
    """
    __author__ = "Nicolas Cuevas"
    __email__ = ""
    __version__ = "1.0"
    __maintainer__ = "Nicolas Cuevas"
    __copyright__ = "Copyright 2016"
    __status__ = "Development"

    id = models.AutoField(primary_key=True, db_index=True)
    name = models.CharField(_('name'), max_length=100, blank=True)
    acronym = models.CharField(_('acronym'), max_length=20, blank=True)
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    birth = models.DateField(_('birth'), default=None, blank=True, null=True)
    country = models.ForeignKey(Country, blank=True,null=True, default=None, verbose_name=_('country'))
    state = models.ForeignKey(Region, blank=True,null=True, default=None, verbose_name=_('state'))
    city = models.CharField(_('city'), max_length=256, default=None, blank=True, null=True)
    zip_code = models.CharField(_('zip code'), max_length=100, default=None, blank=True, null=True)
    address1 = models.CharField(_('address 1'), max_length=200, default=None, blank=True, null=True)
    address2 = models.CharField(_('address 2'), max_length=200, default=None, blank=True, null=True)
    phone1 = models.CharField(_('phone 1'), max_length=50, blank=True)
    phone2 = models.CharField(_('phone 2'), max_length=50, blank=True)
    instructor = models.ForeignKey(Instructor, blank=True,null=True, default=None, verbose_name=_('instructor'))
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    class Meta:
        app_label = 'core'

    def __unicode__(self):
        return self.name
