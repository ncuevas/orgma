# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _

class StudentCategory(models.Model):
    """
    StudentCategory model class.
    """
    __author__ = "Nicolas Cuevas"
    __email__ = ""
    __version__ = "1.0"
    __maintainer__ = "Nicolas Cuevas"
    __copyright__ = "Copyright 2016"
    __status__ = "Development"

    id = models.AutoField(primary_key=True, db_index=True)
    name = models.CharField(_('name'), max_length=100, null=False)
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    class Meta:
        app_label = 'core'

    def __unicode__(self):
        return self.name
