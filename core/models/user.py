# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone
from django.utils.http import urlquote
from django.utils.translation import ugettext_lazy as _
from django.core.mail import send_mail
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from core.models.user_manager import UserManager
from core.models import GENDER
from cities_light.models import Country, Region
from multiselectfield import MultiSelectField



class User(AbstractBaseUser, PermissionsMixin):
    """
    A fully featured User model with admin-compliant permissions that uses
    a full-length email field as the username.

    Email and password are required. Other fields are optional.
    """
    __author__ = "Nicolas Cuevas"
    __email__ = ""
    __version__ = "1.0"
    __maintainer__ = "Nicolas Cuevas"
    __copyright__ = "Copyright 2016"
    __status__ = "Development"


    ROLE = (
        ('ROLE_SUPER_ADMIN', 'Super Admin'),
        ('ROLE_ADMIN', 'Admin'),
        ('ROLE_USER', 'User'),
    )

    username = models.CharField(_('username'), max_length=100, unique=True)
    email = models.EmailField(_('email address'), max_length=254, null=True, blank=True, unique=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    full_name = models.CharField(_('full name'), max_length=90, null=True, blank=True)
    date_of_birth = models.DateField(_('date of birth'), default=None, blank=True, null=True)
    gender = models.CharField(_('gender'), choices=GENDER, max_length=1, default=None, blank=True, null=True)
    country = models.ForeignKey(Country, blank=True,null=True, default=None, verbose_name=_('country'))
    state = models.ForeignKey(Region, blank=True,null=True, default=None, verbose_name=_('state'))
    city = models.CharField(_('city'), max_length=256, default=None, blank=True, null=True)
    zip_code = models.CharField(_('zip code'), max_length=100, default=None, blank=True, null=True)
    address1 = models.CharField(_('address 1'), max_length=200, default=None, blank=True, null=True)
    address2 = models.CharField(_('address 2'), max_length=200, default=None, blank=True, null=True)
    phone1 = models.CharField(_('phone 1'), max_length=50, blank=True)
    phone2 = models.CharField(_('phone 2'), max_length=50, blank=True)
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)
    is_staff = models.BooleanField(_('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin '
                    'site.'))
    is_active = models.BooleanField(_('active'), default=True,
        help_text =_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    # @todo change adding new tables
    role = models.CharField(_('role'), choices=ROLE, max_length=30, default='ROLE_USER',
        help_text=_('Designates whether the user can log into other admin '
                    'sites.'))
    extra_roles = MultiSelectField(_('role'), choices=ROLE, max_choices=3, max_length=30, null=True, blank=True,
        help_text=_('Designates whether the user can access additional features'
                    'sites.'))
    force_change_password = models.BooleanField(_('force change password'), default=False,
        help_text=_('Force change password'))
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    class Meta:
        app_label = 'core'


    def get_absolute_url(self):
        return "/users/%s/" % urlquote(self.username)

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])
