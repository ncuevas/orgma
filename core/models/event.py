# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from core.models import EventStatus, EventType, Document

class Event(models.Model):
    """
    Event model class.
    """
    __author__ = "Nicolas Cuevas"
    __email__ = ""
    __version__ = "1.0"
    __maintainer__ = "Nicolas Cuevas"
    __copyright__ = "Copyright 2016"
    __status__ = "Development"

    id = models.AutoField(primary_key=True, db_index=True)
    name = models.CharField(_('name'), max_length=100, null=False)
    description = models.TextField(_('description'), null=True, blank=True)
    price = models.DecimalField(verbose_name=_('price'), blank=True, default=0, max_digits=8, decimal_places=2)
    status = models.ForeignKey(EventStatus, verbose_name=_('event status'), blank=False, null=False, related_name="event_status")
    type = models.ForeignKey(EventType, verbose_name=_('event type'), blank=False, null=False, related_name="event_type")
    document = models.ForeignKey(Document, verbose_name=_('event document'), blank=True, null=True, default=None, related_name="event_document")
    start_date = models.DateTimeField(_('start date'), auto_now_add=False)
    end_date = models.DateTimeField(_('end date'), auto_now=False, blank=True)
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    class Meta:
        app_label = 'core'

    def __unicode__(self):
        return self.name
