# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from core.models import User, ManagerCategory

class Manager(models.Model):
    """
    Manager model class.
    """
    __author__ = "Nicolas Cuevas"
    __email__ = ""
    __version__ = "1.0"
    __maintainer__ = "Nicolas Cuevas"
    __copyright__ = "Copyright 2016"
    __status__ = "Development"

    id = models.AutoField(primary_key=True, db_index=True)
    member = models.OneToOneField(User, verbose_name=_('member'), null=False)
    category = models.ForeignKey(ManagerCategory, null=False, verbose_name=_('category'))
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    class Meta:
        app_label = 'core'

    def __unicode__(self):
        return self.member.full_name + " - " + self.category.name
