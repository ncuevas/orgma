# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from core.models import Membership, StudentMembershipStatus, Student

class StudentMembership(models.Model):
    """
    StudentMembership model class.
    """
    __author__ = "Nicolas Cuevas"
    __email__ = ""
    __version__ = "1.0"
    __maintainer__ = "Nicolas Cuevas"
    __copyright__ = "Copyright 2016"
    __status__ = "Development"

    student = models.ForeignKey(Student, null=False, verbose_name=_('student'))
    membership = models.ForeignKey(Membership, verbose_name=_('status'), blank=False, null=False, related_name="student_membership")
    status = models.ForeignKey(StudentMembershipStatus, verbose_name=_('status'), blank=False, null=False, related_name="student_membership_status")
    date = models.DateTimeField(_('date'), null=True, blank=True, default=None)

    class Meta:
        app_label = 'core'

    def __unicode__(self):
        return self.membership.name + " - " + self.student.full_name + " - " + self.status.name