# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from core.models import Document, Student, Event, StudentHistoryStatus
from django.utils import timezone
from cities_light.models import Country, Region

class StudentHistory(models.Model):
    """
    StudentHistory model class.
    """
    __author__ = "Nicolas Cuevas"
    __email__ = ""
    __version__ = "1.0"
    __maintainer__ = "Nicolas Cuevas"
    __copyright__ = "Copyright 2016"
    __status__ = "Development"

    id = models.AutoField(primary_key=True, db_index=True)
    student = models.ForeignKey(Student, null=False, verbose_name=_('student'))
    event =  models.ForeignKey(Event, null=False, verbose_name=_('event'))
    status =  models.ForeignKey(StudentHistoryStatus, blank=True,null=True, default=None, verbose_name=_('status'))
    document = models.ForeignKey(Document, verbose_name=_('document'), blank=True, null=True, default=None, related_name="student_history_document")
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(_('updated at'), auto_now=True)

    class Meta:
        app_label = 'core'

    def __unicode__(self):
        return self.event.name + " - " + self.student.full_name + " - " + self.status.name
