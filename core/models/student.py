# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from core.models import User, StudentCategory, Instructor, Organization, Place, GENDER
from cities_light.models import Country, Region

class Student(User):
    """
    Student model class.
    """
    __author__ = "Nicolas Cuevas"
    __email__ = ""
    __version__ = "1.0"
    __maintainer__ = "Nicolas Cuevas"
    __copyright__ = "Copyright 2016"
    __status__ = "Development"

    category = models.ForeignKey(StudentCategory, verbose_name=_('category'), null=False)
    instructor = models.ForeignKey(Instructor, blank=True,null=True, default=None, verbose_name=_('instructor'), related_name="student_instructor")
    organization = models.ForeignKey(Organization, null=False, verbose_name=_('organization'))
    place = models.ForeignKey(Place, blank=True, null=True, default=None, verbose_name=_('institute'))

    class Meta:
        app_label = 'core'

    def __unicode__(self):
        return self.full_name
