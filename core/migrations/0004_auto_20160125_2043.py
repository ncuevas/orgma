# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_studenthistory_document'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='studentexam',
            name='student',
        ),
        migrations.AddField(
            model_name='document',
            name='student',
            field=models.ForeignKey(default=None, verbose_name='student', to='core.Student'),
        ),
    ]
