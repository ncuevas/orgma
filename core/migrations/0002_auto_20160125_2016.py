# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='StudentMembership',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(default=None, null=True, verbose_name='date', blank=True)),
                ('membership', models.ForeignKey(related_name='student_membership', verbose_name='status', to='core.Membership')),
            ],
        ),
        migrations.CreateModel(
            name='StudentMembershipStatus',
            fields=[
                ('id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='name')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
            ],
        ),
        migrations.AddField(
            model_name='studentexam',
            name='student',
            field=models.ForeignKey(default=None, verbose_name='student', to='core.Student'),
        ),
        migrations.AddField(
            model_name='studentmembership',
            name='status',
            field=models.ForeignKey(related_name='student_membership_status', verbose_name='status', to='core.StudentMembershipStatus'),
        ),
        migrations.AddField(
            model_name='studentmembership',
            name='student',
            field=models.ForeignKey(verbose_name='student', to='core.Student'),
        ),
    ]
