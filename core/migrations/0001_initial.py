# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import multiselectfield.db.fields
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
        ('cities_light', '0003_auto_20141120_0342'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(unique=True, max_length=100, verbose_name='username')),
                ('email', models.EmailField(max_length=254, unique=True, null=True, verbose_name='email address', blank=True)),
                ('first_name', models.CharField(max_length=30, verbose_name='first name', blank=True)),
                ('last_name', models.CharField(max_length=30, verbose_name='last name', blank=True)),
                ('full_name', models.CharField(max_length=90, null=True, verbose_name='full name', blank=True)),
                ('date_of_birth', models.DateField(default=None, null=True, verbose_name='date of birth', blank=True)),
                ('gender', models.CharField(default=None, choices=[(b'm', b'Male'), (b'f', b'Female')], max_length=1, blank=True, null=True, verbose_name='gender')),
                ('city', models.CharField(default=None, max_length=256, null=True, verbose_name='city', blank=True)),
                ('zip_code', models.CharField(default=None, max_length=100, null=True, verbose_name='zip code', blank=True)),
                ('address1', models.CharField(default=None, max_length=200, null=True, verbose_name='address 1', blank=True)),
                ('address2', models.CharField(default=None, max_length=200, null=True, verbose_name='address 2', blank=True)),
                ('phone1', models.CharField(max_length=50, verbose_name='phone 1', blank=True)),
                ('phone2', models.CharField(max_length=50, verbose_name='phone 2', blank=True)),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('role', models.CharField(default=b'ROLE_USER', help_text='Designates whether the user can log into other admin sites.', max_length=30, verbose_name='role', choices=[(b'ROLE_SUPER_ADMIN', b'Super Admin'), (b'ROLE_ADMIN', b'Admin'), (b'ROLE_USER', b'User')])),
                ('extra_roles', multiselectfield.db.fields.MultiSelectField(choices=[(b'ROLE_SUPER_ADMIN', b'Super Admin'), (b'ROLE_ADMIN', b'Admin'), (b'ROLE_USER', b'User')], max_length=30, blank=True, help_text='Designates whether the user can access additional featuressites.', null=True, verbose_name='role')),
                ('force_change_password', models.BooleanField(default=False, help_text='Force change password', verbose_name='force change password')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
            ],
        ),
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('note', models.TextField(default=None, null=True, verbose_name='note', blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
            ],
        ),
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='name')),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('price', models.DecimalField(default=0, verbose_name='price', max_digits=8, decimal_places=2, blank=True)),
                ('start_date', models.DateTimeField(verbose_name='start date')),
                ('end_date', models.DateTimeField(verbose_name='end date', blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
            ],
        ),
        migrations.CreateModel(
            name='EventStatus',
            fields=[
                ('id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='name', blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
            ],
        ),
        migrations.CreateModel(
            name='EventType',
            fields=[
                ('id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='name', blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
            ],
        ),
        migrations.CreateModel(
            name='Instructor',
            fields=[
                ('id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
            ],
        ),
        migrations.CreateModel(
            name='Manager',
            fields=[
                ('id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
            ],
        ),
        migrations.CreateModel(
            name='ManagerCategory',
            fields=[
                ('id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='name')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
            ],
        ),
        migrations.CreateModel(
            name='Membership',
            fields=[
                ('id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='name', blank=True)),
                ('description', models.TextField(null=True, verbose_name='description', blank=True)),
                ('start_date', models.DateTimeField(verbose_name='start date')),
                ('end_date', models.DateTimeField(verbose_name='end date', blank=True)),
                ('price', models.DecimalField(default=0, verbose_name='price', max_digits=8, decimal_places=2, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='MembershipStatus',
            fields=[
                ('id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='name', blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
            ],
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='name', blank=True)),
                ('acronym', models.CharField(max_length=20, verbose_name='acronym', blank=True)),
                ('birth', models.DateField(default=None, null=True, verbose_name='birth', blank=True)),
                ('city', models.CharField(default=None, max_length=256, null=True, verbose_name='city', blank=True)),
                ('zip_code', models.CharField(default=None, max_length=100, null=True, verbose_name='zip code', blank=True)),
                ('address1', models.CharField(default=None, max_length=200, null=True, verbose_name='address 1', blank=True)),
                ('address2', models.CharField(default=None, max_length=200, null=True, verbose_name='address 2', blank=True)),
                ('phone1', models.CharField(max_length=50, verbose_name='phone 1', blank=True)),
                ('phone2', models.CharField(max_length=50, verbose_name='phone 2', blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
                ('country', models.ForeignKey(default=None, blank=True, to='cities_light.Country', null=True, verbose_name='country')),
                ('state', models.ForeignKey(default=None, blank=True, to='cities_light.Region', null=True, verbose_name='state')),
            ],
        ),
        migrations.CreateModel(
            name='Place',
            fields=[
                ('id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='name', blank=True)),
                ('acronym', models.CharField(max_length=20, verbose_name='acronym', blank=True)),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('birth', models.DateField(default=None, null=True, verbose_name='birth', blank=True)),
                ('city', models.CharField(default=None, max_length=256, null=True, verbose_name='city', blank=True)),
                ('zip_code', models.CharField(default=None, max_length=100, null=True, verbose_name='zip code', blank=True)),
                ('address1', models.CharField(default=None, max_length=200, null=True, verbose_name='address 1', blank=True)),
                ('address2', models.CharField(default=None, max_length=200, null=True, verbose_name='address 2', blank=True)),
                ('phone1', models.CharField(max_length=50, verbose_name='phone 1', blank=True)),
                ('phone2', models.CharField(max_length=50, verbose_name='phone 2', blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
                ('country', models.ForeignKey(default=None, blank=True, to='cities_light.Country', null=True, verbose_name='country')),
                ('instructor', models.ForeignKey(default=None, blank=True, to='core.Instructor', null=True, verbose_name='instructor')),
                ('state', models.ForeignKey(default=None, blank=True, to='cities_light.Region', null=True, verbose_name='state')),
            ],
        ),
        migrations.CreateModel(
            name='StudentCategory',
            fields=[
                ('id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='name')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
            ],
        ),
        migrations.CreateModel(
            name='StudentExamStatus',
            fields=[
                ('id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='name')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
            ],
        ),
        migrations.CreateModel(
            name='StudentHistory',
            fields=[
                ('id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
                ('event', models.ForeignKey(verbose_name='event', to='core.Event')),
            ],
        ),
        migrations.CreateModel(
            name='StudentHistoryStatus',
            fields=[
                ('id', models.AutoField(db_index=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='name', blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='created at')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='updated at')),
            ],
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('user_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('category', models.ForeignKey(verbose_name='category', to='core.StudentCategory')),
            ],
            bases=('core.user',),
        ),
        migrations.CreateModel(
            name='StudentExam',
            fields=[
                ('document_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='core.Document')),
                ('date', models.DateTimeField(default=None, null=True, verbose_name='date', blank=True)),
                ('status', models.ForeignKey(related_name='student_exam_status', verbose_name='status', to='core.StudentExamStatus')),
            ],
            bases=('core.document',),
        ),
        migrations.AddField(
            model_name='studenthistory',
            name='status',
            field=models.ForeignKey(default=None, blank=True, to='core.StudentHistoryStatus', null=True, verbose_name='status'),
        ),
        migrations.AddField(
            model_name='membership',
            name='status',
            field=models.ForeignKey(related_name='membership_status', verbose_name='membership status', to='core.MembershipStatus'),
        ),
        migrations.AddField(
            model_name='manager',
            name='category',
            field=models.ForeignKey(verbose_name='category', to='core.ManagerCategory'),
        ),
        migrations.AddField(
            model_name='manager',
            name='member',
            field=models.OneToOneField(verbose_name='member', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='instructor',
            name='student',
            field=models.OneToOneField(verbose_name='student', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='event',
            name='document',
            field=models.ForeignKey(related_name='event_document', default=None, blank=True, to='core.Document', null=True, verbose_name='event document'),
        ),
        migrations.AddField(
            model_name='event',
            name='status',
            field=models.ForeignKey(related_name='event_status', verbose_name='event status', to='core.EventStatus'),
        ),
        migrations.AddField(
            model_name='event',
            name='type',
            field=models.ForeignKey(related_name='event_type', verbose_name='event type', to='core.EventType'),
        ),
        migrations.AddField(
            model_name='user',
            name='country',
            field=models.ForeignKey(default=None, blank=True, to='cities_light.Country', null=True, verbose_name='country'),
        ),
        migrations.AddField(
            model_name='user',
            name='groups',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', verbose_name='groups'),
        ),
        migrations.AddField(
            model_name='user',
            name='state',
            field=models.ForeignKey(default=None, blank=True, to='cities_light.Region', null=True, verbose_name='state'),
        ),
        migrations.AddField(
            model_name='user',
            name='user_permissions',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions'),
        ),
        migrations.AddField(
            model_name='studenthistory',
            name='student',
            field=models.ForeignKey(verbose_name='student', to='core.Student'),
        ),
        migrations.AddField(
            model_name='student',
            name='instructor',
            field=models.ForeignKey(related_name='student_instructor', default=None, blank=True, to='core.Instructor', null=True, verbose_name='instructor'),
        ),
        migrations.AddField(
            model_name='student',
            name='organization',
            field=models.ForeignKey(verbose_name='organization', to='core.Organization'),
        ),
        migrations.AddField(
            model_name='student',
            name='place',
            field=models.ForeignKey(default=None, blank=True, to='core.Place', null=True, verbose_name='institute'),
        ),
    ]
