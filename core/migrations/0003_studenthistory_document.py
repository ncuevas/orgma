# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20160125_2016'),
    ]

    operations = [
        migrations.AddField(
            model_name='studenthistory',
            name='document',
            field=models.ForeignKey(related_name='student_history_document', default=None, blank=True, to='core.Document', null=True, verbose_name='document'),
        ),
    ]
