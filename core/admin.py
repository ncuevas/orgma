from django.contrib import admin
from django.contrib.auth.models import Group
from simple_history.admin import SimpleHistoryAdmin
from core.models import *
# Register your models here.
admin.site.register(User)
admin.site.register(Organization)
#admin.site.register(ManagerCategory)
admin.site.register(Manager)
admin.site.register(Instructor)
admin.site.register(Place)
#admin.site.register(StudentCategory)
admin.site.register(Student)
admin.site.register(Document)
admin.site.register(Event)
#admin.site.register(EventStatus)
#admin.site.register(EventType)
#admin.site.register(MembershipStatus)
admin.site.register(Membership)
#admin.site.register(StudentMembershipStatus)
admin.site.register(StudentMembership)
#admin.site.register(StudentExamStatus)
admin.site.register(StudentExam)
#admin.site.register(StudentHistoryStatus)
admin.site.register(StudentHistory)
